# Installation du projet

## Installer les logiciels
[nodejs](https://nodejs.org/en/download) et [docker desktop](https://www.docker.com/get-started/)

## Installer les packages
> ### Pour les utilisateurs Windows : Assurez vous de lancer ces commandes avec **Power Shell**

- Allez dans le dossier api : `npm install`

- Allez dans le dossier front : `npm install`

## Lancer le projet

- A la racine du projet : `docker-compose up -d`

## Installation du certificat

Le projet est dorénavant en **HTTPS** avec vérification d'un certificat côté client. Si le client ne présente pas avec
un certificat approuvé par l'autorité de certification du serveur, il n'a pas accès à l'application.

1.
   - (avec Firefox) Aller dans **Setttings > Privacy & Security > View Certificates (scroll down to see it)**, onglet **"Your Certificates"**
   - (avec Chrome) Aller dans **Paramètres > Confidentialité et sécurité > Sécurité > Gérer les certificats d'appareils**

2. Cliquer sur **" Import "** et sélectionner le fichier **"front/SSL
   Features/CLT_cert_firefox.p12"**

3. Taper le mot de passe "lol1234" et cliquer sur **"Sign in"**

4. Cliquer sur **"Ok"** et fermer l'onglet

Lors de votre connexion à la page web d'authentification [https://localhost:3000](https://localhost:3000), une pop-up *
*"User Identification Request"** apparaît. Cocher sur **"Remember this decision"** et cliquer sur **"OK"**.

Sélectionner par la suite, toujours sur la page web, **"Advanced"** et **"Accept the risk and continue"**.

La page d'authentification devrait alors apparaître.

## BDD
Sous PhpMyAdmin ou avec un autre utilitaire (les ports sont en dessous), créer une base "libelluleErgo" et importer sa structure et données grâce au fichier api/db_migrations/libelluleErgo.sql
apres la création de la BDD, il faut faire un arret/relance du conteneur Front avant de se connecter à ce dernier

## Urls
- Front : [https://localhost:3000](https://localhost:3000)

- Back : [http://localhost:4000](http://localhost:4000)

- PhpMyAdmin: [http://localhost:8080](http://localhost:8080)

- MariaDB: [http://localhost:3306](http://localhost:3306)

## Frameworks utilisés

Framework JS: ReactJS, NodeJS

CSS : [tailwindcss](https://tailwindcss.com/docs/installation)

Framework components : [chakra-ui](https://chakra-ui.com/getting-started)

Formulaires : [formik](https://formik.org/docs/api/formik)

Icones : https://react-icons.github.io/react-icons/icons?name=hi

Enjoy :)

## Tests et vérifications

A la fin de la mise en place de la partie logistique, on peut faire plusieurs tests :
- tester la connexion à l'application en créant un compte sur le front.
- **(sous Windows)** tester des modifications en temps reel sur l'application : 
   - lancer la commande npm run dev via le terminal dans le repertoire front (il doit rester toujours ouvert)
   - se connecter sur l'url affichée par le terminal (https://localhost:3001)

#### Ressources 📚

A titre indicatif, concernant la configuration :

* de l'autorité de certification (CA) hébergée sur le serveur
* du certificat du serveur signé par la CA
* du certificat du client signé par la CA

Vous référez aux commandes ci-dessous :

``` bash
# Create CA for 5 years
openssl req -x509 -new -nodes -key CA_key.key -sha256 -days 1825 -out CA_cert.pem

# Configure the certificate of the server and generate the private key associated (valid for 1 year)
openssl req -new -nodes -out SRV_cert.csr -newkey rsa:4096 -keyout SRV_key.key 

cat > SRV_cert.v3.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = RD_test.local
DNS.2 = RD_test_dev.local
IP.1 = 192.168.1.1
EOF

openssl x509 -req -in SRV_cert.csr -CA CA_cert.pem -CAkey CA_key.key -CAcreateserial -out SRV_cert.crt -days 365 -sha256 -extfile SRV_cert.v3.ext

# Configure the certificate of the client and generate the private key associated (valid for 1 year)

openssl req -new -nodes -out CLT_cert.csr -newkey rsa:4096 -keyout CLT_key.key 

openssl x509 -req -in CLT_cert.csr -CA CA_cert.pem -CAkey CA_key.key -CAcreateserial -out CLT_cert.crt -days 365 -sha256

# Read the certificate

openssl x509 -in SRV_cert.crt -noout -text
openssl x509 -in CLT_cert.crt -noout -text
openssl x509 -in CA_cert.pem -noout -text

# Convert certificate in .crt to .p12 in order to import it in web browser

openssl pkcs12 -export -in CLT_cert.crt -inkey CLT_key.key -out CLT_cert_firefox.p12
```

