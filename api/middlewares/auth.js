const jwt = require('jsonwebtoken');
const secretKey = 'the-super-strong-secrect'; // Assure-toi de stocker cela de manière sécurisée

const auth = (req, res, next) => {
    const token = req.headers?.['authorization']?.slice(7);

    if (!token) {
        return res.status(401).json({ message: 'Token missing' });
    }

    try {
        const decoded = jwt.verify(token, secretKey);
        req.user = decoded;
        next();
    } catch (error) {
        return res.status(401).json({ message: 'Token invalid' });
    }
};

module.exports = auth;