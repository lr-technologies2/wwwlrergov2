RENAME TABLE `libelluleErgo`.`patient_assist` TO `libelluleErgo`.`patient_helper`;
ALTER TABLE `patient_helpers` CHANGE `fk_patient_id` `patient_id` INT(11) NOT NULL COMMENT 'users table id with role = 1', CHANGE `fk_assist_id` `helper_id` INT(11) NOT NULL COMMENT 'users table id with role = 2';

RENAME TABLE `libelluleErgo`.`patient_sensor` TO `libelluleErgo`.`patient_sensors`;
ALTER TABLE `patient_sensors` CHANGE `fk_users_id` `user_id` INT(11) NOT NULL, CHANGE `fk_sensor_id` `sensor_id` INT(11) NOT NULL;

RENAME TABLE `libelluleErgo`.`role` TO `libelluleErgo`.`roles`;
RENAME TABLE `libelluleErgo`.`role_straight` TO `libelluleErgo`.`role_rights`;
ALTER TABLE `role_rights` CHANGE `fk_role_id` `role_id` INT(11) NOT NULL, CHANGE `fk_straight_id` `right_id` INT(11) NOT NULL;

RENAME TABLE `libelluleErgo`.`sensor` TO `libelluleErgo`.`sensors`;
ALTER TABLE `sensors` CHANGE `fk_sensor_status_id` `sensor_status_id` INT(11) NOT NULL, CHANGE `fk_sensor_type_id` `sensor_type_id` INT(11) NOT NULL;

RENAME TABLE `libelluleErgo`.`sensor_type` TO `libelluleErgo`.`sensor_types`;

RENAME TABLE `libelluleErgo`.`straight` TO `libelluleErgo`.`rights`;
ALTER TABLE `rights` CHANGE `straight_create` `create` TINYINT(1) NULL DEFAULT NULL, CHANGE `straight_read` `read` TINYINT(1) NULL DEFAULT NULL, CHANGE `straight_update` `update` TINYINT(1) NULL DEFAULT NULL, CHANGE `straight_delete` `delete` TINYINT(1) NULL DEFAULT NULL;

ALTER TABLE `users` CHANGE `fk_role_id` `role_id` INT(11) NOT NULL;
ALTER TABLE `users` DROP `pseudo`;
UPDATE `roles` SET `name` = 'helper' WHERE `roles`.`id` = 2;