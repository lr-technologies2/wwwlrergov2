const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const usersRouter = require('./routers/users/index');

const app = express();

app.use(express.json());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

app.use('/api', usersRouter);

// Handling Errors
app.use((err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error";
    res.status(err.statusCode).json({
        message: err.message,
    });
});

app.listen(4000, () => console.log('Server is running on port 4000'));