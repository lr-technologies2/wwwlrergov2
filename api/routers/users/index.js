const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const db = require('../../dbConnection');
const {signupValidation, loginValidation} = require("../../validations");
const auth = require('../../middlewares/auth');


// REGISTER USER
router.post('/register', signupValidation, (req, res, next) => {
    db.query(
        `SELECT *
         FROM users
         WHERE LOWER(email) = LOWER(${db.escape(
                 req.body.email
         )});`,

        (err, result) => {
            if (result.length) {
                return res.status(409).send({
                    msg: 'Compte déjà existant'
                });
            } else {
                // username is available
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).send({
                            msg: err
                        });
                    } else {
                        // has hashed pw => add to database
                        db.query(
                            `INSERT INTO users (role_id, first_name, last_name, email, password)
                             VALUES ('${req.body.role_id}', '${req.body.first_name}', '${req.body.last_name}',
                                     '${req.body.email}', '${hash}')`,
                            (err) => {
                                if (err) {
                                    return res.status(400).send({
                                        msg: err
                                    });
                                }
                                return res.status(201).send({
                                    msg: "L'utilisateur a bien été créé",
                                    create: true
                                });
                            }
                        );
                    }
                });
            }
        }
    );
});

router.put('/user/block-helper', auth, (req, res, next) => {
    const userId = req.user.id;
    const helperId = req.body.helperId;

    const sql = `UPDATE patient_helper SET blocked = true WHERE helper_id = ? AND patient_id = ?`;
    
    db.query(sql, [helperId, userId], (err, result) => {
        if (err) {
            return res.status(500).send({
                error: 'Erreur lors de la mise à jour du statut dans la table patient_helper.',
            });
        }

        return res.status(200).send({
            helpers: result,
        });
    });
});

router.put('/user/unblock-helper', auth, (req, res, next) => {
    const userId = req.user.id;
    const helperId = req.body.helperId;

    const sql = 'UPDATE patient_helper SET blocked = false WHERE helper_id = ? AND patient_id = ?';
    
    db.query(sql, [helperId, userId], (err, result) => {
        if (err) {
            return res.status(500).send({
                error: 'Erreur lors de la mise à jour du statut dans la table patient_helper.',
            });
        }

        return res.status(200).send({
            helpers: result,
        });
    });
});

router.put('/user/:id', signupValidation, (req, res, next) => {
    const id = req.params.id;
    const updateData = req.body;
    const sql = `UPDATE users
                 SET ?
                 WHERE id = ?`;

    bcrypt.hash(req.body.password, 10, (err, hash) => {
        db.query(sql, [{...updateData, password: hash}, id], () => {
            return res.status(200).send({
                msg: 'Votre compte a bien été mis à jour !',
                user: {...req.body, id: id},
                update: true
            });
        });
    })
});

router.get('/user/:id', (req, res, next) => {
    const id = req.params.id;
    const sql = `SELECT *
                 FROM users
                 WHERE id = ${id}`;

    db.query(sql, (err, result) => {
        return res.status(200).send({
            user: result[0],
        });
    });
});


// USER LOGIN
router.post('/login', loginValidation, (req, res, next) => {
    db.query(
        `SELECT *
         FROM users
         WHERE email = ${db.escape(req.body.email)};`,
        (err, result) => {
            // user does not exists
            if (err) {
                return res.status(400).send({
                    msg: err
                });
            }
            if (!result.length) {
                return res.status(401).send({
                    msg: 'Mail et/ou mot de passe incorrect !'
                });
            }
            // check password
            bcrypt.compare(
                req.body.password,
                result[0]['password'],
                (bErr, bResult) => {
                    // wrong password
                    if (bErr) {
                        return res.status(401).send({
                            msg: 'Mail et/ou mot de passe incorrect !'
                        });
                    }
                    if (bResult) {
                        const token = jwt.sign({id: result[0].id}, 'the-super-strong-secrect', {expiresIn: '1h'});

                        const {password, ...rest} = result[0];

                        return res.status(200).send({
                            msg: 'Logged in!',
                            login: true,
                            token,
                            user: rest
                        });
                    }
                    return res.status(401).send({
                        msg: 'Mail et/ou mot de passe incorrect !'
                    });
                }
            );
        }
    );
});

// get helpers by user ID
router.get('/helpers', auth, (req, res, next) => {
    const userId = req.user.id

    const sql = `SELECT u.id, u.first_name, u.last_name, ph.start_date, ph.end_date, ph.blocked FROM patient_helper AS ph JOIN users AS u ON ph.helper_id = u.ID WHERE ph.patient_id = ${userId}`;
    db.query(sql, (err, result) => {
        if (err) {
            console.error(err);
            return res.status(500).send({
                error: 'Une erreur s\'est produite lors de la récupération des aidants.',
            });
        }
        
        return res.status(200).send({
            helpers: result,
        });
    });
});

router.get('/helpers/available', auth, (req, res, next) => {
    const userId = req.user.id

    const sql = `SELECT id, role_id, last_name, first_name, avatar FROM users WHERE role_id = 2 AND NOT EXISTS (SELECT 1 FROM patient_helper WHERE helper_id = users.id AND patient_id = ${userId})`;
    db.query(sql, (err, result) => {
        if (err) {
            return res.status(500).send({
                error: 'Une erreur s\'est produite lors de la récupération des aidants.',
            });
        }
        
        return res.status(200).send({
            helpers: result,
        });
    });
});

router.delete('/user/unlink-helper', auth, (req, res, next) => {
    const userId = req.user.id
    const sql = 'DELETE FROM patient_helper WHERE helper_id = ? AND patient_id = ?'
    db.query(sql, [req.body.helperId, userId], (err, result) => {
        return res.status(200).send({
            helpers: result,
        });
    })
})

router.delete('/user/:id', signupValidation, (req, res, next) => {
    const id = req.params.id;
    const sql = [`DELETE FROM patient_helper WHERE patient_id = ${id} OR helper_id = ${id}`, `DELETE FROM patient_sensors WHERE user_id = ${id}`, `DELETE FROM users WHERE id = ${id}`];
    db.query(sql.join(";"), (err, result) => {
        return res.status(200).send({
            msg: 'Votre compte a bien été supprimé !',
            deleted: true
        });
    });
});

router.post('/user/link-to-helper', auth, (req, res, next) => {
    const userId = req.user.id
    const sql = `INSERT INTO patient_helper (helper_id, patient_id) VALUES (${req.body.helperId}, ${userId})`
    db.query(sql, (err, result) => {
        return res.status(200).send({
            helpers: result,
        });
    })
})

module.exports = router;