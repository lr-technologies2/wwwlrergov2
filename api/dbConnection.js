const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'db', // Replace with your host name
    user: 'root',      // Replace with your database username
    password: 'azerty',      // Replace with your database password
    database: 'libelluleErgo', // Replace with your database Name
    multipleStatements: true,
});


connection.connect(function (err, result) {
    if (err) console.log(err);
    else console.log('Database is connected successfully !');
});

module.exports = connection;