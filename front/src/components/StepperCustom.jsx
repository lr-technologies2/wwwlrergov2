import {
    Box,
    Button,
    HStack,
    Step,
    StepDescription,
    StepIndicator,
    Stepper,
    StepSeparator,
    StepStatus,
    StepTitle,
    useSteps,
} from '@chakra-ui/react'
import {useFormik} from "formik";
import IdentificationForm from "../forms/sensor/IdentificationForm.jsx";

const StepperCustom = () => {


    const formStepper = useFormik({
        initialValues: {
            sensorName: ""
        },
        onSubmit: (values) => {
            console.log(values);
        }
    });

    const steps = [
        {title: 'Etape 1', description: 'Identification', component: <IdentificationForm formStepper={formStepper}/>},
        {title: 'Etape 2', description: 'Nommage', component: <h1>Component 2</h1>},
        {title: 'Etape 3', description: 'Repérage', component: <h1>Component 3</h1>},
        {title: 'Etape 4', description: 'Calibrage', component: <h1>Component 4</h1>},
    ]

    const {activeStep, goToNext, goToPrevious, activeStepPercent} = useSteps({
        index: 0,
        count: steps.length,
    })


    return (
        <>
            <Stepper index={activeStep}>
                {steps.map((step, index) => (
                    <Step key={index}>
                        <StepIndicator>
                            <StepStatus
                                complete={`✅`}
                                incomplete={`😅`}
                                active={`📍`}
                            />
                        </StepIndicator>

                        <Box flexShrink='0'>
                            <StepTitle>{step.title}</StepTitle>
                            <StepDescription>{step.description}</StepDescription>
                        </Box>

                        <StepSeparator/>
                    </Step>
                ))}
            </Stepper>
            <form className='p-5 pl-0 mt-5'>
                <Box maxW='lg' maxH='lg'>
                    {steps[activeStep].component}
                </Box>
                <HStack mt={5} className='absolute bottom-5'>

                    {activeStepPercent > 0.33 &&
                        <Button onClick={() => goToPrevious()}> Précédent</Button>
                    }

                    {activeStepPercent === 1 ?
                        <Button onClick={() => formStepper.handleSubmit()}> Ajouter</Button> :
                        <Button onClick={() => goToNext()}> Suivant</Button>
                    }
                </HStack>
            </form>
        </>
    )
}

export default StepperCustom