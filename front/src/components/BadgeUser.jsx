import {useNavigate} from "react-router-dom";
import {Role} from "../utils/Role.js";
import {Avatar, Badge, Flex, Text} from "@chakra-ui/react";
import {useThemeContext} from "../theme/ThemeContext.jsx";
import { useUserLocalStorage } from "../hook/useUserLocalStorage.js";

const BadgeUser = () => {

    let navigate = useNavigate();

    const {background} = useThemeContext();

    const [user, ] = useUserLocalStorage();

    return <Flex
        style={{background: background}}
        className={`transition ${background} ease-in-out delay-150 items-center p-3 cursor-pointer`}
        onClick={() => navigate('/profil')}
    >
        <Avatar/>
        <Flex ml='3'>
            <Text fontWeight='bold' className='mr-2'>
                Bonjour,
            </Text>
            <Text>
                {user?.first_name + " " + user?.last_name}
            </Text>
            <Badge ml='5' colorScheme='green' className='flex items-center'>
                {Role(+user?.role_id)}
            </Badge>
        </Flex>
    </Flex>
}
export default BadgeUser;