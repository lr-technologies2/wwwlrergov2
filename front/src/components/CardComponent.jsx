import { Badge, Button, Card, CardBody, CardFooter, CardHeader, Heading, Text } from "@chakra-ui/react";
import { useThemeContext } from "../theme/ThemeContext.jsx";
import { Link } from "react-router-dom";

const CardComponent = ({ count, title, description, icon, button, link }) => {

    const { backgroundPrimary, backgroundSecondary } = useThemeContext();

    return <Card
        className={`transition ease-in-out delay-150 ${backgroundPrimary} hover:${backgroundSecondary} text-white`}>
        <CardHeader>
            {icon}
            <Heading size='lg'><Badge className='p-3 rounded mr-3 text-xl'>{count}</Badge>{title}</Heading>
        </CardHeader>
        <CardBody>
            <Text>{description}</Text>
        </CardBody>
        <CardFooter>
            <Link to={link}> <Button className='bg-pink-500 text-white hover:bg-pink-700'>{button}</Button> </Link>
        </CardFooter>
    </Card>;
}

export default CardComponent