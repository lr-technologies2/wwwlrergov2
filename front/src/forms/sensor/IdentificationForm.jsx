import {FormControl, Input, InputGroup,} from "@chakra-ui/react";

const IdentificationForm = ({formStepper}) =>
    <FormControl>
        <InputGroup size='lg'>
            <Input
                type="text"
                placeholder="nom du capteur"
                name="sensorName"
                onChange={formStepper.handleChange}
                value={formStepper.values?.sensorName}
            />
        </InputGroup>
    </FormControl>

export default IdentificationForm;