import {
    Button,
    chakra,
    FormControl,
    FormHelperText,
    Image,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Link,
    Stack,
} from "@chakra-ui/react";
import {useEffect, useState} from "react";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";
import {useNavigate} from "react-router-dom";
import { useLocalStorage } from "@uidotdev/usehooks";
import { useUserLocalStorage } from "../../hook/useUserLocalStorage.js";
import { LocalStorageKey } from "../../utils/LocalStorageKey.js";
import { useLoginMutation } from "../../services/endpoints/user.js";

const LoginUserForm = () => {
  // TODO: Enhance token security by considering storage options such as HttpOnly cookies or server sessions.
  const [token, setToken] = useLocalStorage(LocalStorageKey.TOKEN)
  const [, setUser] = useUserLocalStorage()

  const CFaUserAlt = chakra(FaUserAlt);
  const CFaLock = chakra(FaLock);

  const [showPassword, setShowPassword] = useState(false);
  const handleShowClick = () => setShowPassword(!showPassword);

  const navigate = useNavigate();

  const [login, { isLoading, data }] = useLoginMutation()

  const form = useFormik({
      initialValues: {
        email: "",
        password: ""
      },
      onSubmit: (values) => {
        login(values)
      }
  });

   useEffect(() => {
        if (data) {
          setToken(data.token)
          setUser(data.user)
        } else {
          setToken(undefined)
          setUser(undefined)
        }
    }, [data, setToken, setUser])
    useEffect(() => {
      if (token) {
        navigate('/dashboard')
      }
    }, [token, navigate])

    return <form onSubmit={form.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            backgroundColor="whiteAlpha.900"
            boxShadow="md"
            alignItems="center"
        >
            <Image
                boxSize='150px'
                align='center'
                objectFit='cover'
                src='./logo.png'
                alt='Dan Abramov'
            />
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        placeholder="Mail"
                        name="email"
                        required
                        onChange={form.handleChange}
                        value={form.values?.email}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Mot de passe"
                        name="password"
                        required
                        onChange={form.handleChange}
                        value={form.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
                <FormHelperText textAlign="right" mt='5'>
                    <Link>Mot de passe oublié ?</Link>
                </FormHelperText>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                className="bg-pink-500 text-white hover:bg-pink-700"
                width="full"
                isLoading={isLoading}
            >
                Se connecter
            </Button>
        </Stack>
    </form>
}

export default LoginUserForm;