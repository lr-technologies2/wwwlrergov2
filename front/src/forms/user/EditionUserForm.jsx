import {
    Button,
    chakra,
    FormControl,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Select,
    Stack,
    useToast,
    FormLabel
} from "@chakra-ui/react";
import {HELPING, PATIENT} from "../../utils/Role.js";
import {useEffect, useState} from "react";
import useApi from "../../hook/useApi.jsx";
import {useNavigate} from "react-router-dom";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";
import { useUserLocalStorage } from "../../hook/useUserLocalStorage.js";

const RegisterForm = () => {

    const CFaUserAlt = chakra(FaUserAlt);
    const CFaLock = chakra(FaLock);
    const [showPassword, setShowPassword] = useState(false);
    const handleShowClick = () => setShowPassword(!showPassword);

    const {deleteUser, editionUser, isEditionUser} = useApi();

    const [user, ] = useUserLocalStorage();

    const toast = useToast();

    const navigate = useNavigate();

    const formSignIn = useFormik({
        initialValues: {
            last_name: user.last_name,
            first_name: user.first_name,
            role_id: user.role_id,
            email: user.email,
            password: user.password
        },
        validate: (values) => {
            const errors = {};

            // Validations prénom
            if (!values.first_name.trim()) {
                errors.first_name = "Le prénom est requis";
            } else if (values.first_name.trim().length < 2) {
                  errors.first_name = "Le prénom doit contenir minimum 2 caractères";
            } else if (!/^[a-zA-ZÀ-ÿ\s'-]*$/.test(values.first_name.trim())) {
                  errors.first_name = "Le prénom doit seulement contenir des lettres";
            }

            // Validations nom
            if (!values.last_name.trim()) {
                errors.last_name = "Le nom est requis";
              } else if (values.last_name.trim().length < 2) {
                errors.last_name = "Le nom doit contenir minimum 2 caractères";
            } else if (!/^[a-zA-ZÀ-ÿ\s'-]*$/.test(values.last_name.trim())) {
                errors.last_name = "Le nom doit seulement contenir des lettres";
            } 


             // Validations email
             if (!values.email.trim()) {
                errors.email = "L'email est requis";
            } else if (
            !/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(values.email.trim())
            ) {
                errors.email = "Le format de l'email est invalide";
            }

            return errors;

  
        },
        onSubmit: (values) => {
            editionUser(values, user?.id).then((r) => r);
        }
    });

    useEffect(() => {
        isEditionUser && toast({
            title: isEditionUser ? 'Votre compte a bien été mis à jour' : 'Error',
            status: isEditionUser ? 'success' : 'error',
            duration: 1000,
            isClosable: true,
        })
        setTimeout(() => isEditionUser ? navigate('/dashboard') : "", 2000)
    }, [isEditionUser])

    return <><form onSubmit={formSignIn.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            alignItems="center"
        >
            <FormControl pb={5}>
                <FormLabel>Nom</FormLabel>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        name="last_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.last_name}
                    />
                </InputGroup>
                {formSignIn.touched.last_name && formSignIn.errors.last_name && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.last_name}</div>
                )}
            </FormControl>
            <FormControl pb={5}>
                <FormLabel>Prénom</FormLabel>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        name="first_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.first_name}
                    />
                </InputGroup>
                {formSignIn.touched.first_name && formSignIn.errors.first_name && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.first_name}</div>
                )}
            </FormControl>
            <FormControl pb={5}>
                <FormLabel>Role</FormLabel>
                <Select size='md' name='role_id' onChange={formSignIn.handleChange}
                        value={formSignIn.values?.role_id}>
                    <option value={PATIENT}>Patient</option>
                    <option value={HELPING}>Aidant</option>
                </Select>
            </FormControl>
            <FormControl pb={5}>
                <FormLabel>Mail</FormLabel>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        name="email"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.email}
                    />
                </InputGroup>
                {formSignIn.touched.email && formSignIn.errors.email && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.email}</div>
                )}
            </FormControl>
            <FormControl pb={5}>
                <FormLabel>Mot de passe</FormLabel>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        name="password"
                        required
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                className="bg-pink-500 text-white hover:bg-pink-700"
                width="full"
            >
                Mettre à jour son profil
            </Button>
        </Stack>
    </form>
    <div className={'m-4 my-0'}>
        <Button
            borderRadius={0}
            type="submit"
            variant="solid"
            colorScheme="red"
            width="full"
            onClick={() => deleteUser(user.id)}
        >
            Supprimer son profil
        </Button>
    </div>
    </>
}

export default RegisterForm;