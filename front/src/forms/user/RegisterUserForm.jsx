import {
    Button,
    chakra,
    FormControl,
    Image,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Select,
    Stack,
    useToast
} from "@chakra-ui/react";
import {HELPING, PATIENT} from "../../utils/Role.js";
import {useEffect, useState} from "react";
import useApi from "../../hook/useApi.jsx";
import {useNavigate} from "react-router-dom";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";

const RegisterUserForm = () => {

    const CFaUserAlt = chakra(FaUserAlt);
    const CFaLock = chakra(FaLock);
    const [showPassword, setShowPassword] = useState(false);
    const handleShowClick = () => setShowPassword(!showPassword);

    const {register, isRegister} = useApi();
    const toast = useToast();

    const navigate = useNavigate();

    const formSignIn = useFormik({
        initialValues: {
            last_name: "",
            first_name: "",
            role_id: PATIENT,
            email: "",
            password: ""
        },
        validate: (values) => {
            const errors = {};

            // Validations prénom
            if (!values.first_name.trim()) {
              errors.first_name = "Le prénom est requis";
            } else if (values.first_name.trim().length < 2) {
                errors.first_name = "Le prénom doit contenir minimum 2 caractères";
            } else if (!/^[a-zA-ZÀ-ÿ\s'-]*$/.test(values.first_name.trim())) {
                errors.first_name =
                  "Le prénom doit seulement contenir des lettres";
            } 

            // Validations nom
            if (!values.last_name.trim()) {
                errors.last_name = "Le nom est requis";
              } else if (values.last_name.trim().length < 2) {
                errors.last_name = "Le nom doit contenir minimum 2 caractères";
            } else if (!/^[a-zA-ZÀ-ÿ\s'-]*$/.test(values.last_name.trim())) {
                errors.last_name =
                  "Le nom doit seulement contenir des lettres";
            } 

            // Validations email
            if (!values.email.trim()) {
                errors.email = "L'email est requis";
            } else if (
            !/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(values.email.trim())
            ) {
                errors.email = "Le format de l'email est invalide";
            }

            return errors;
        },
        onSubmit: (values) => {
            register(values).then((r) => r);
        }
    });

    useEffect(() => {
        isRegister && toast({
            title: isRegister ? 'Votre compte a bien été créé' : 'Error',
            description: isRegister ? "Le compte a bien été créé" : "lors de la création de votre compte",
            status: isRegister ? 'success' : 'error',
            duration: 1000,
            isClosable: true,
        })
        setTimeout(() => isRegister ? navigate('/login') : "", 2000)
    }, [isRegister])

    return <form onSubmit={formSignIn.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            backgroundColor="whiteAlpha.900"
            boxShadow="md"
            alignItems="center"
        >
            <Image
                boxSize='150px'
                align='center'
                objectFit='cover'
                src='./logo.png'
                alt='Dan Abramov'
            />
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="Nom"
                        name="last_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.lastName}
                    />
                </InputGroup>
                {formSignIn.touched.last_name && formSignIn.errors.last_name && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.last_name}</div>
                )}
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="Prénom"
                        name="first_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.firstName}
                    />
                </InputGroup>
                {formSignIn.touched.first_name && formSignIn.errors.first_name && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.first_name}</div>
                )}
            </FormControl>
            <FormControl>
                <Select size='md' name='role_id' onChange={formSignIn.handleChange}
                        value={formSignIn.values?.role_id}>
                    <option disabled>Rôle</option>
                    <option value={PATIENT}>Patient</option>
                    <option value={HELPING}>Aidant</option>
                </Select>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        placeholder="Email"
                        name="email"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.email}
                    />
                </InputGroup>
                {formSignIn.touched.email && formSignIn.errors.email && (
                    <div className="text-red-700 px-2 py-2" role="alert">{formSignIn.errors.email}</div>
                )}
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Password"
                        name="password"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                className="bg-pink-500 text-white hover:bg-pink-700"
                width="full"
            >
                S'inscrire
            </Button>
        </Stack>
    </form>
}

export default RegisterUserForm;