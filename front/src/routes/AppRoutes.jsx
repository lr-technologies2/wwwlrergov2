import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import App from "../App.jsx";
import Dashboard from "../pages/Dashboard.jsx";
import Sensors from "../pages/Sensors.jsx";
import Profil from "../pages/Profil.jsx";
import Login from "../pages/Login.jsx";
import Register from "../pages/Register.jsx";
import PrivateRoutes from "./PrivateRoutes.jsx";
import Helpers from "../pages/Helpers.jsx";
import { Provider } from "react-redux";
import { store } from "../store/store.js";

const AppRoutes = () =>

<Provider store={store}>
    <BrowserRouter>
        <Routes>
            <Route element={<PrivateRoutes/>}>
                <Route path='/' element={<Navigate to='login' replace/>}/>
                <Route
                    path="/"
                    element={<App/>}
                >
                    <Route path="dashboard" element={<Dashboard/>}/>
                    <Route path="sensors" element={<Sensors/>}/>
                    <Route path="profil" element={<Profil/>}/>
                    <Route path="helpers" element={<Helpers/>}/>
                </Route>
            </Route>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
        </Routes>
    </BrowserRouter>
</Provider>

export default AppRoutes;