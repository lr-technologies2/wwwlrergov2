import {Navigate, Outlet} from "react-router-dom";
import { useUserLocalStorage } from "../hook/useUserLocalStorage";

const PrivateRoutes = () => {
  const [user, ] = useUserLocalStorage();
  return user ? <Outlet/> : <Navigate to={'/login'}/>
}

export default PrivateRoutes;