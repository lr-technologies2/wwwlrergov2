import React, {useContext} from "react";
import {theme} from "./theme.js";


const ThemeContext = React.createContext(theme);
export const useThemeContext = () => useContext(ThemeContext);

export const ThemeProvider = ({children}) => {
    return (
        <ThemeContext.Provider
            value={theme}>
            {children}
        </ThemeContext.Provider>
    );
};