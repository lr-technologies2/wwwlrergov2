import { Table, Thead, Tbody, Tr, Th, Td, TableContainer, Button, Flex, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter, useDisclosure } from "@chakra-ui/react";
import { useLazyGetHelpersAvailableQuery, useGetHelpersQuery } from "../services/endpoints/helpers";
import { useBlockHelperMutation, useLinkToHelperMutation, useUnblockHelperMutation, useUnlinkHelperMutation } from "../services/endpoints/user";
import { useEffect } from "react";
import { DateUtils } from "../utils/DateUtils";

const Helpers = () => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { data: helpers } = useGetHelpersQuery()
    const [unlinkHelper] = useUnlinkHelperMutation()
    const [blockHelper] = useBlockHelperMutation()
    const [unblockHelper] = useUnblockHelperMutation()

    const blockButtonClickHander = (helper) => {
      if (helper?.blocked) {
        unblockHelper({ helperId: helper.id })
      } else {
        blockHelper({ helperId: helper.id })
      }
    }

    return (
      <>
      <Flex justifyContent="flex-end">
        <Button size="sm" colorScheme="teal" onClick={onOpen}>Ajouter un aidant</Button>
      </Flex>
      <TableContainer>
          <Table variant="simple">
              <Thead>
                  <Tr>
                      <Th>ID</Th>
                      <Th>Nom</Th>
                      <Th>Prénom</Th>
                      <Th>Date début</Th>
                      <Th>Date fin</Th>
                      <Th isNumeric>Actions</Th>
                  </Tr>
              </Thead>
              <Tbody>
                  {helpers?.helpers?.map((helper) => (
                    <Tr key={helper.id} className={helper.blocked ? 'helper-blocked' : ''}>
                        <Td >{helper.id}</Td>
                        <Td>{helper.first_name}</Td>
                        <Td>{helper.last_name}</Td>
                        <Td>{DateUtils.formatDateToFr(helper.start_date)}</Td>
                        <Td>{DateUtils.formatDateToFr(helper.end_date)}</Td>
                        <Td>
                          <Flex 
                            gap={2}
                            justify="flex-end"
                          >
                            <Button size="sm" colorScheme="gray" onClick={() => blockButtonClickHander(helper)}>{helper?.blocked ? 'Débloquer' : 'Bloquer'}</Button>
                            <Button size="sm" colorScheme="red" onClick={() => unlinkHelper({ helperId: helper.id })}>Supprimer</Button>
                          </Flex>
                        </Td>
                    </Tr>
                  ))}
              </Tbody>
          </Table>
      </TableContainer>
      <HelpersModal onClose={onClose} isOpen={isOpen} />
      </>
    );
};

const HelpersModal = ({ onClose, isOpen}) => {
  const [getHelpersAvailable, { data: helpersAvailable }] = useLazyGetHelpersAvailableQuery()
  const [linkToHelper] = useLinkToHelperMutation()

  useEffect(() => {
    if (isOpen) {
      getHelpersAvailable()
    }
  }, [isOpen, getHelpersAvailable])

  return (
      <Modal onClose={onClose} size="full" isOpen={isOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Liste des aidants disponibles</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
          <TableContainer>
          <Table variant="simple">
              <Thead>
                  <Tr>
                      <Th>ID</Th>
                      <Th>Nom</Th>
                      <Th>Prénom</Th>
                      <Th isNumeric>Actions</Th>
                  </Tr>
              </Thead>
              <Tbody>
                {helpersAvailable?.helpers?.map(helper => (
                  <Tr key={helper.id}>
                    <Td >{helper.id}</Td>
                    <Td>{helper.first_name}</Td>
                    <Td>{helper.last_name}</Td>
                    <Td>
                      <Flex justifyContent="flex-end">
                        <Button size="sm" onClick={() => linkToHelper({ helperId: helper.id })} colorScheme="teal">Ajouter</Button>
                      </Flex>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
          </ModalBody>
        </ModalContent>
      </Modal>
  )
}

export default Helpers