import { Center, Divider, SimpleGrid} from "@chakra-ui/react";
import CardComponent from "../components/CardComponent.jsx";
import {HiOutlineUserGroup, HiStatusOnline} from "react-icons/hi";
import BadgeUser from "../components/BadgeUser.jsx";
import {Role} from "../utils/Role.js";
import { useUserLocalStorage } from "../hook/useUserLocalStorage.js";
import { useGetHelpersQuery } from "../services/endpoints/helpers.js";

export const Dashboard = () => {

    const { data: helpers } = useGetHelpersQuery()
    const [user, ] = useUserLocalStorage();

    return (
        <>
            <BadgeUser/>
            <div className='w-100'>
                <Center height='50px'>
                    <Divider/>
                </Center>
                <SimpleGrid spacing={4} templateColumns='repeat(auto-fill, minmax(400px, 1fr))'>
                    <CardComponent
                        count={helpers?.helpers?.length ?? 0}
                        title={Role(+user?.role_id) === 'Patient' ? 'Aidants' : 'Patients'}
                        description={Role(+user?.role_id) === 'Patient' ? 'Retrouver le nombre d\'aidants' : 'Retrouver le nombre de patients'}
                        button={'Consulter'}
                        link = {'/helpers'}
                        icon={<HiOutlineUserGroup size={'15em'} className='absolute right-5 opacity-20'/>}/>
                    <CardComponent
                        count={5}
                        title={'Capteurs'}
                        description={'Retrouver le nombre de capteurs'}
                        button={'Consulter'}
                        icon={<HiStatusOnline size={'15em'} className='absolute right-5 opacity-20'/>}
                    />
                </SimpleGrid>
            </div>
        </>
    );
}

export default Dashboard;