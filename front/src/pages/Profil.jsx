import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import EditionUserForm from "../forms/user/EditionUserForm.jsx";

export const Profil = () => {

    return (
        <Tabs>
            <TabList>
                <Tab>Edition du profil</Tab>
                <Tab>Gérer ses aidants</Tab>
            </TabList>

            <TabPanels>
                <TabPanel>
                    <EditionUserForm/>
                </TabPanel>
                <TabPanel>
                    <p>Gestion des aidants</p>
                </TabPanel>
            </TabPanels>
        </Tabs>
    );
}

export default Profil;