import { api } from '../api'

export const helpersApiSlice = api.injectEndpoints({
  endpoints: builder => ({
    getHelpers: builder.query({
      query: () => '/helpers',
      providesTags: ['Helpers'],
    }),
    getHelpersAvailable: builder.query({
      query: () => '/helpers/available',
      providesTags: ['Helpers']
    }),
  }),
})

export const { 
  useGetHelpersQuery,
  useLazyGetHelpersAvailableQuery
} = helpersApiSlice