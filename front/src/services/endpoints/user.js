import { api } from '../api'

const userApi =  api.injectEndpoints({
  endpoints: builder => ({
    login: builder.mutation({
      query: (credentials) => ({
        url: 'login',
        method: 'POST',
        body: credentials,
      }),
      providesTags: ['User']
    }),
    linkToHelper: builder.mutation({
      query: (helper) => ({
        url: '/user/link-to-helper',
        method: 'POST',
        body: helper
      }),
      providesTags: ['Helpers'],
      invalidatesTags: ['Helpers']
    }),
    unlinkHelper: builder.mutation({
      query: (helper) => ({
        url: '/user/unlink-helper',
        method: 'DELETE',
        body: helper
      }),
      providesTags: ['Helpers'],
      invalidatesTags: ['Helpers']
    }),
    blockHelper: builder.mutation({
      query: (helper) => ({
        url: '/user/block-helper',
        method: 'PUT',
        body: helper
      }),
      providesTags: ['Helpers'],
      invalidatesTags: ['Helpers']
    }),
    unblockHelper: builder.mutation({
      query: (helper) => ({
        url: '/user/unblock-helper',
        method: 'PUT',
        body: helper
      }),
      providesTags: ['Helpers'],
      invalidatesTags: ['Helpers']
    }),
  }),
})

export const { 
  useLoginMutation,
  useLinkToHelperMutation,
  useUnlinkHelperMutation,
  useBlockHelperMutation,
  useUnblockHelperMutation
} = userApi