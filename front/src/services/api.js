import { fetchBaseQuery } from "@reduxjs/toolkit/query";
import { createApi } from "@reduxjs/toolkit/query/react";
import { LocalStorageKey } from "../utils/LocalStorageKey";

// TODO : To add inside a .env file.
const API_URL = 'http://localhost:4000/api'

export const api = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({ 
    baseUrl: API_URL,
    prepareHeaders: (headers, ) => {
      const tokenText = window.localStorage.getItem(LocalStorageKey.TOKEN) 
      const token = tokenText ? JSON.parse(tokenText) : null;

      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }

      return headers;
    }
  }),
  tagTypes: ['Helpers', 'User'],
  endpoints: () => ({}),
  refetchOnMountOrArgChange: 1
})
