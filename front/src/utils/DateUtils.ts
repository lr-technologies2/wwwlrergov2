export class DateUtils {
  static formatDateToFr(date) {
    return (new Date(date)).toLocaleDateString('fr')
  }
}