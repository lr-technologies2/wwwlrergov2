export const PATIENT = 1
export const HELPING = 2

export const Role = (role) => {
    switch(role){
        case PATIENT:
            return 'Patient'
        case HELPING:
            return 'Aidants'
    }
}