import { useLocalStorage } from "@uidotdev/usehooks"
import { LocalStorageKey } from "../utils/LocalStorageKey"

export const useUserLocalStorage = () => {
  const [userLocal, setUserLocal] = useLocalStorage(LocalStorageKey.USER)

  const setUser = (user) => {
    if (user) {
      setUserLocal(JSON.stringify(user))
    }
  }
  const user = userLocal ? JSON.parse(userLocal) : undefined;
  return [user, setUser]
}