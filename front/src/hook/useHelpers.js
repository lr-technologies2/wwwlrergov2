import { useEffect, useState } from "react";
import useApi from "./useApi.jsx";

const useHelpers = (userId) => {
    const [helpers, setHelpers] = useState([]);
    const { helpersByUserID } = useApi();

    useEffect(() => {
        helpersByUserID(userId)
            .then((data) => {
                setHelpers(data);
            })
            .catch((error) => {
                console.error("Erreur lors de la récupération des aidants :", error);
            });
    }, [userId]);

    return helpers;
};
export default useHelpers;