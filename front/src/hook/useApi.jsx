import {useState} from 'react';
import {useToast} from "@chakra-ui/react";
import {useNavigate} from "react-router-dom";

const useApi = () => {

    const [isRegister, setIsRegister] = useState(false);
    const [isEditionUser, setIsEditionUser] = useState(false);
    const [isLogin, setIsLogin] = useState(false);
    const navigate = useNavigate();

    const toast = useToast();
    const register = (registerForm) => {
        return fetch("http://localhost:4000/api/register",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(registerForm)
            })
            .then((res) => res.json())
            .then(data => {
                if (data.create) {
                    setIsRegister(true)
                } else {
                    toast({
                        title: data.msg,
                        status: 'error',
                        duration: 1000,
                        isClosable: true,
                    })
                    setIsRegister(false);
                }
            })
            .catch((res) => console.log(res))
    }

    const editionUser = (registerForm, userId) => {
        return fetch("http://localhost:4000/api/user/" + userId,
            {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(registerForm)
            })
            .then((res) => res.json())
            .then(data => {
                if (data.update) {
                    setIsEditionUser(true);
                    console.log(data);
                    localStorage.clear();
                    localStorage.setItem('user', JSON.stringify(data.user));
                } else {
                    toast({
                        title: data.msg,
                        status: 'error',
                        duration: 1000,
                        isClosable: true,
                    })
                    setIsEditionUser(false);
                }
            })
            .catch((res) => console.log(res))
    }

    const login = (registerForm) => {
        return fetch("http://localhost:4000/api/login",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(registerForm)
            })
            .then((res) => res.json())
            .then(data => {
                if (data.login) {
                    setIsLogin(true)
                    localStorage.clear();
                    localStorage.setItem('user', JSON.stringify(data.user));
                } else {
                    toast({
                        title: data.msg,
                        status: 'error',
                        duration: 1000,
                        isClosable: true,
                    })
                    setIsLogin(false);
                }
            })
            .catch((res) => console.log(res))
    }

    const userCurrent = (id) => {
        return fetch("http://localhost:4000/api/user/" + id,
            {
                method: "GET"
            })
            .then((res) => res.json())
            .then(data => data.user)
            .catch((res) => console.log(res))
    }

    const deleteUser = (id) => {
        return fetch("http://localhost:4000/api/user/" + id,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE"
            })
            .then((res) => res.json())
            .then((data) => {
                if(data.deleted){
                     toast({
                        title: 'Votre compte a bien été supprimé',
                        status: 'success',
                        duration: 3000,
                        isClosable: true,
                    })
                    localStorage.clear();
                    navigate('/login');
                }

            })
            .catch((res) => console.log(res))
    }

    const helpersByUserID = (id) => {
        return fetch("http://localhost:4000/api/user/" + id + "/helpers", {
            method: "GET"
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Erreur lors de la requête.");
                }
                return response.json();
            })
            .then((data) => data.helpers)
            .catch((error) => {
                console.error("Erreur de requête :", error);
            });
    };

    return {
        deleteUser,
        editionUser,
        register,
        login,
        userCurrent,
        helpersByUserID,
        isEditionUser,
        isRegister,
        isLogin
    }
}

export default useApi;